import os, sys
from os.path import dirname as _

workspace = _(_(__file__))
project = os.path.join(workspace, 'gentoo_ads')
sys.path.append(workspace)
sys.path.append(project)

# Set up settings module
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
