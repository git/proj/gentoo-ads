# -*- coding: utf-8 -*-
# most of this stuff is obvious.
# tier is not currently used
# weight is whatever integer you want, i.e. bogomips, etc

# note: all images must be referenced with a path relative to MEDIA_URL
# so for example if we want to point at:
# www.example.com/static/imgs/some/path/some/where/my_image.jpg
# and our MEDIA_URL is set to www.example.com/static/ then we would want
# 'name': 'imgs/some/path/some/where/my_image.jpg'
# Note that the MEDIA_URL has a trailing slash !important


## try for lang/country first, then just lang, then fallback to en

ads = [
	{
		'type':'html',
		'name': 'internal_name_1',
		'title': 
		{
			'en':'human title 1',
			'it':'italian human title 1',
		},
		'tier': 1,
		'weight': 15,									#bogomips
		'url': 'http://www1.example.com',
		'html': '<h2>Heading</h2><p>This <a href="http://www.example.com">here</a> is a link.</p>',
		'height': 120,
		'width': 125,
	},
	{
		'type':'txt',
		'name': 'internal_name_2',
		'title': 'human title 2',
		'tier': 1,
		'weight': 25,
		'url': 'http://www2.example.com',
		'text': 
		{
			'en':'On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain.',
			'de':'german text translation',
			'it':'italian trans of num 2'
		},
		'height': 120,
		'width': 125,
	},
	{
		'type':'img',
		'name': 'internal_name_3',
		'title': 
		{
			'en':'human title 3',
			'de':'german 3',
			'it':'italian 3',
			'de-DE':'de-DE german 3',
		},
		'tier': 1,
		'weight': 5,
		'url': 'http://www3.example.com',
		'img': 'path/to/img3.jpg',
		'height': 120,
		'width': 125,
	},
	{
		'type':'img',
		'name': 'internal_name_4',
		'title': 'human title 4',
		'tier': 2,
		'weight': 105,
		'url': 'http://www4.example.com',
		'img': 'path/to/img4.png',
		'height': 120,
		'width': 125,
	},
]
