# -*- coding: utf-8 -*-
import logging

from logging.handlers import SysLogHandler

LOGGING_DEBUG = True

SYS_LOG_ADDRESS = '/dev/log'

## Put in an identifying string like gentoo-ads
## This facilitates filtering with syslog
AD_LOG_PREFIX = 'gentoo-ads'

if not hasattr(logging, "set_up_done"):
	logging.set_up_done=False

def setup_logging():

	if logging.set_up_done:
		return


	logger = logging.getLogger()
	if LOGGING_DEBUG:
		logger.setLevel(logging.DEBUG)
	else:
		logger.setLevel(logging.INFO)

	formatter = logging.Formatter(""+AD_LOG_PREFIX+" %(levelname)s %(message)s")

	# test console logger
	if LOGGING_DEBUG:
		handler = logging.StreamHandler()
		handler.setLevel(logging.DEBUG)
		handler.setFormatter(formatter)
		logger.addHandler(handler)

	# SysLogHandler
	SysLogAddress = SYS_LOG_ADDRESS
	if SysLogAddress is not None:
		handler = SysLogHandler(SysLogAddress)
		handler.setLevel(logging.INFO)
		handler.setFormatter(formatter)
		logger.addHandler(handler)

	#########################
	# LOGGING LEVELS
	#  * indicates a custom level
	#
	# CRITICAL	50
	# ERROR			40
	# WARNING		30
	# INFO			20
	# DEBUG			10
	# NOTSET		0
	#



	logging.set_up_done=True

setup_logging()
