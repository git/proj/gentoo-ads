# -*- coding: utf-8 -*-
# Django settings for gentoo_ads project.

## created by Matthew Summers aka quantumsummers for Gentoo Linux
## I suppose gplv3 is a nice license for this software, so be it.
## If someone else every uses this, feel free to contact me via
## quantumsummers at the gentoo dot org domain.

from imp import load_source
import os
from logging_setup import setup_logging, AD_LOG_PREFIX

## this is the path to the file containing your advertiser dictionaries.
## please note, in general the ads.py file should live outside the webroot.
CONFIG_PATH = '/some/path/towards/the/file/gentoo-ads/example_ads.py'

## PLEASE NOTE: You need to setup three variables in logging_setup.py, namely LOGGING_DEBUG, AD_LOG_PREFIX , and SYS_LOG_ADDRESS
## IF LOGGING_DEBUG = True we log to both the console (for debugging) and syslog
##AD_LOG_PREFIX  = ''some-identifying-string' to facilitate filtering log messages with syslog
## SYS_LOG_ADDRESS is simply the log file you wish to use, in general and since we want to use the SysLogHandler, we desire to log to /dev/log



## nifty, facilitates use of advertiser dictionary
## loads a python module living somwhere on the machine
## though if it finds a good .pyc|o it will use it first
ads_module = load_source('ads_module', CONFIG_PATH,)

## sets the number of ads to be displayed 
ADS_LENGTH = 3

## why is this not above ADS_LENGTH? 
## list of dictionaries we use in the view, i,e, the advertisements.
ADS_STRUCT = ads_module.ads

## this should really be an absolute path in production,
## also eliminating the `import os` above
ADS_IMAGES_DIR = os.path.join(os.path.dirname(CONFIG_PATH), 'images')

## These are the translateable keys in the ads_struct.
TRANS_KEYS = ('html', 'title', 'text')

#default language for ads
DEFAULT_ADS_LANG = 'en'

DEBUG = True
TEMPLATE_DEBUG = DEBUG

## this will email errors when DEBUG=FALSE
ADMINS = ()
    # ('Your Name', 'your_email@domain.com'),


MANAGERS = ADMINS

## needs no DB at this point
#DATABASE_ENGINE = ''           # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
#DATABASE_NAME = ''             # Or path to database file if using sqlite3.
#DATABASE_USER = ''             # Not used with sqlite3.
#DATABASE_PASSWORD = ''         # Not used with sqlite3.
#DATABASE_HOST = ''             # Set to empty string for localhost. Not used with sqlite3.
#DATABASE_PORT = ''             # Set to empty string for default. Not used with sqlite3.

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
#MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"

## this is hypothecal at this time
MEDIA_URL = 'http://media.example.com/img/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'v1nht7uj01_btjz4!&2%8(xq!$gao%80&r1w=h#ij17*@+-$3k'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = ()

MIDDLEWARE_CLASSES = ()

ROOT_URLCONF = 'gentoo_ads.urls'

## not needed using the above template loader
TEMPLATE_DIRS = ()
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.


## we have but one
## app this day
## perhaps another 
## tomorrow
INSTALLED_APPS = (
	'ads',
)
